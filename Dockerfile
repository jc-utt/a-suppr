FROM quay.io/jitesoft/node

WORKDIR /app

COPY package.json package.json

RUN npm i

COPY . .

EXPOSE 8080

CMD ["node", "server.js"]